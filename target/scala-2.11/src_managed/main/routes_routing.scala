// @SOURCE:/Users/sagarbendale/Documents/workspace/pms/conf/routes
// @HASH:7227a4db4a5308cb9ce1440ea6b9e615585f950e
// @DATE:Tue Mar 24 18:17:11 PDT 2015


import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString

object Routes extends Router.Routes {

import ReverseRouteContext.empty

private var _prefix = "/"

def setPrefix(prefix: String): Unit = {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Application_index0_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
private[this] lazy val controllers_Application_index0_invoker = createInvoker(
controllers.Application.index(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
        

// @LINE:8
private[this] lazy val controllers_Application_form1_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("form"))))
private[this] lazy val controllers_Application_form1_invoker = createInvoker(
controllers.Application.form(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "form", Nil,"GET", """""", Routes.prefix + """form"""))
        

// @LINE:10
private[this] lazy val controllers_Application_login2_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
private[this] lazy val controllers_Application_login2_invoker = createInvoker(
controllers.Application.login(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "login", Nil,"GET", """""", Routes.prefix + """login"""))
        

// @LINE:12
private[this] lazy val controllers_Application_postLogin3_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("authenticate"))))
private[this] lazy val controllers_Application_postLogin3_invoker = createInvoker(
controllers.Application.postLogin(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "postLogin", Nil,"POST", """""", Routes.prefix + """authenticate"""))
        

// @LINE:14
private[this] lazy val controllers_Application_postForm4_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("post"))))
private[this] lazy val controllers_Application_postForm4_invoker = createInvoker(
controllers.Application.postForm(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "postForm", Nil,"POST", """""", Routes.prefix + """post"""))
        

// @LINE:16
private[this] lazy val controllers_Application_dashboard5_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("dashboard"))))
private[this] lazy val controllers_Application_dashboard5_invoker = createInvoker(
controllers.Application.dashboard(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "dashboard", Nil,"GET", """""", Routes.prefix + """dashboard"""))
        

// @LINE:18
private[this] lazy val controllers_Application_logout6_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
private[this] lazy val controllers_Application_logout6_invoker = createInvoker(
controllers.Application.logout(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "logout", Nil,"GET", """""", Routes.prefix + """logout"""))
        

// @LINE:20
private[this] lazy val controllers_Application_allManagers7_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("manager"))))
private[this] lazy val controllers_Application_allManagers7_invoker = createInvoker(
controllers.Application.allManagers(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "allManagers", Nil,"GET", """""", Routes.prefix + """manager"""))
        

// @LINE:22
private[this] lazy val controllers_Application_addManager8_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("manager/add"))))
private[this] lazy val controllers_Application_addManager8_invoker = createInvoker(
controllers.Application.addManager(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "addManager", Nil,"GET", """""", Routes.prefix + """manager/add"""))
        

// @LINE:24
private[this] lazy val controllers_Application_postAddManager9_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("manager/add"))))
private[this] lazy val controllers_Application_postAddManager9_invoker = createInvoker(
controllers.Application.postAddManager(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "postAddManager", Nil,"POST", """""", Routes.prefix + """manager/add"""))
        

// @LINE:26
private[this] lazy val controllers_Application_addData10_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("data/add"))))
private[this] lazy val controllers_Application_addData10_invoker = createInvoker(
controllers.Application.addData(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "addData", Nil,"GET", """""", Routes.prefix + """data/add"""))
        

// @LINE:29
private[this] lazy val controllers_Assets_at11_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
private[this] lazy val controllers_Assets_at11_invoker = createInvoker(
controllers.Assets.at(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """form""","""controllers.Application.form()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.login()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """authenticate""","""controllers.Application.postLogin()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """post""","""controllers.Application.postForm()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """dashboard""","""controllers.Application.dashboard()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""controllers.Application.logout()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """manager""","""controllers.Application.allManagers()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """manager/add""","""controllers.Application.addManager()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """manager/add""","""controllers.Application.postAddManager()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """data/add""","""controllers.Application.addData()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]]
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Application_index0_route(params) => {
   call { 
        controllers_Application_index0_invoker.call(controllers.Application.index())
   }
}
        

// @LINE:8
case controllers_Application_form1_route(params) => {
   call { 
        controllers_Application_form1_invoker.call(controllers.Application.form())
   }
}
        

// @LINE:10
case controllers_Application_login2_route(params) => {
   call { 
        controllers_Application_login2_invoker.call(controllers.Application.login())
   }
}
        

// @LINE:12
case controllers_Application_postLogin3_route(params) => {
   call { 
        controllers_Application_postLogin3_invoker.call(controllers.Application.postLogin())
   }
}
        

// @LINE:14
case controllers_Application_postForm4_route(params) => {
   call { 
        controllers_Application_postForm4_invoker.call(controllers.Application.postForm())
   }
}
        

// @LINE:16
case controllers_Application_dashboard5_route(params) => {
   call { 
        controllers_Application_dashboard5_invoker.call(controllers.Application.dashboard())
   }
}
        

// @LINE:18
case controllers_Application_logout6_route(params) => {
   call { 
        controllers_Application_logout6_invoker.call(controllers.Application.logout())
   }
}
        

// @LINE:20
case controllers_Application_allManagers7_route(params) => {
   call { 
        controllers_Application_allManagers7_invoker.call(controllers.Application.allManagers())
   }
}
        

// @LINE:22
case controllers_Application_addManager8_route(params) => {
   call { 
        controllers_Application_addManager8_invoker.call(controllers.Application.addManager())
   }
}
        

// @LINE:24
case controllers_Application_postAddManager9_route(params) => {
   call { 
        controllers_Application_postAddManager9_invoker.call(controllers.Application.postAddManager())
   }
}
        

// @LINE:26
case controllers_Application_addData10_route(params) => {
   call { 
        controllers_Application_addData10_invoker.call(controllers.Application.addData())
   }
}
        

// @LINE:29
case controllers_Assets_at11_route(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at11_invoker.call(controllers.Assets.at(path, file))
   }
}
        
}

}
     