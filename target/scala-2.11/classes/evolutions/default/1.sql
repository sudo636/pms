# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table data (
  id                        integer auto_increment not null,
  drive_name                varchar(255),
  name                      varchar(255),
  description               varchar(255),
  constraint pk_data primary key (id))
;

create table manager (
  id                        integer auto_increment not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  email                     varchar(255),
  contact                   varchar(255),
  constraint pk_manager primary key (id))
;

create table request (
  id                        bigint auto_increment not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  emp_id                    integer,
  contact                   varchar(255),
  constraint pk_request primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  username                  varchar(255),
  password                  varchar(255),
  name                      varchar(255),
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id))
;


create table manager_data (
  manager_id                     integer not null,
  data_id                        integer not null,
  constraint pk_manager_data primary key (manager_id, data_id))
;



alter table manager_data add constraint fk_manager_data_manager_01 foreign key (manager_id) references manager (id) on delete restrict on update restrict;

alter table manager_data add constraint fk_manager_data_data_02 foreign key (data_id) references data (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table data;

drop table manager;

drop table manager_data;

drop table request;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

