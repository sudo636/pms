
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object login extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,Boolean,User,Form[views.formdata.LoginForm],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(page: String, isLoggedIn: Boolean, userInfo: User,loginForm: Form[views.formdata.LoginForm]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._
import views.html.bootstrap3._

Seq[Any](format.raw/*1.95*/("""

"""),format.raw/*5.1*/("""	 
"""),_display_(/*6.2*/main(page, isLoggedIn)/*6.24*/ {_display_(Seq[Any](format.raw/*6.26*/("""

    

"""),format.raw/*10.1*/("""<div class="panel panel-default">

  <div class="panel-heading">
    <h3 class="panel-title">Admin Login</h3>
  </div>
  <div class="panel-body">
   	"""),_display_(/*16.6*/form(routes.Application.postLogin(), 'class -> "form-horizontal")/*16.71*/{_display_(Seq[Any](format.raw/*16.72*/("""
    	 """),format.raw/*17.7*/("""<!-- Name -->
  		"""),_display_(/*18.6*/text(loginForm("username"),
        	label = "Username",
        	placeholder = "username",
        	help="Help username")),format.raw/*21.31*/("""
        	
    	"""),_display_(/*23.7*/password(loginForm("password"),
        	label = "Password",
        	placeholder = "Password")),format.raw/*25.35*/("""
        	
       	"""),format.raw/*27.9*/("""<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button id="submit" type="submit" value="Submit" class="btn btn-primary">Login</button>
	    </div>
	  </div>
    """)))}),format.raw/*32.6*/("""
  """),format.raw/*33.3*/("""</div>
</div>
		
    
""")))}),format.raw/*37.2*/("""
"""))}
  }

  def render(page:String,isLoggedIn:Boolean,userInfo:User,loginForm:Form[views.formdata.LoginForm]): play.twirl.api.HtmlFormat.Appendable = apply(page,isLoggedIn,userInfo,loginForm)

  def f:((String,Boolean,User,Form[views.formdata.LoginForm]) => play.twirl.api.HtmlFormat.Appendable) = (page,isLoggedIn,userInfo,loginForm) => apply(page,isLoggedIn,userInfo,loginForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Tue Mar 24 18:31:44 PDT 2015
                  SOURCE: /Users/sagarbendale/Documents/workspace/pms/app/views/login.scala.html
                  HASH: f6141325cdd2baed8ffc2d17a8a729551901433b
                  MATRIX: 767->1|994->94|1022->145|1051->149|1081->171|1120->173|1155->181|1332->332|1406->397|1445->398|1479->405|1524->424|1667->546|1710->563|1826->658|1872->677|2094->869|2124->872|2177->895
                  LINES: 26->1|30->1|32->5|33->6|33->6|33->6|37->10|43->16|43->16|43->16|44->17|45->18|48->21|50->23|52->25|54->27|59->32|60->33|64->37
                  -- GENERATED --
              */
          