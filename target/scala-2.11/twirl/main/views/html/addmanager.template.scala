
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object addmanager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,Boolean,User,Form[views.formdata.ManagerForm],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(page: String, isLoggedIn: Boolean, userInfo: User, managerForm: Form[views.formdata.ManagerForm]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._
import views.html.bootstrap3._

Seq[Any](format.raw/*1.100*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/main(page, isLoggedIn)/*5.24*/ {_display_(Seq[Any](format.raw/*5.26*/("""
	"""),format.raw/*6.2*/("""<h2>"""),_display_(/*6.7*/page),format.raw/*6.11*/("""</h2>
	<fieldset>
	    <div class="">
		    """),_display_(/*9.8*/form(routes.Application.postAddManager(), 'class -> "form-horizontal")/*9.78*/{_display_(Seq[Any](format.raw/*9.79*/("""
		    	 """),format.raw/*10.9*/("""<!-- Name -->
		  		"""),_display_(/*11.8*/text(managerForm("firstName"),
		        	label = "First Name",
		        	placeholder = "first name (required)")),format.raw/*13.50*/("""
	       		"""),_display_(/*14.12*/text(managerForm("lastName"),
		        	label = "Last Name",
		        	placeholder = "last name (required)")),format.raw/*16.49*/("""
	        	"""),_display_(/*17.12*/text(managerForm("email"),
		        	label = "Email",
		        	placeholder = "Manager's email ID (required)")),format.raw/*19.58*/("""
		        	
		        	"""),format.raw/*21.12*/("""<div class="control-group">
          <label class="control-label" for="majors">Major(s)</label>
          <div class="controls">
            <select multiple="multiple">
              <option>Chemistry</option>
              <option>Biology</option>
              <option>Physics</option>
              <option>Other</option>
            </select>
            <p class="help-block">Select one or more majors.</p>
          </div>
        </div>
		        	
	        	"""),_display_(/*34.12*/text(managerForm("contact"),
		        	label = "Manager's Contact #",
		        	placeholder = "contact number")),format.raw/*36.43*/("""
	        	"""),format.raw/*37.11*/("""<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button id="submit" type="submit" value="Submit" class="btn btn-primary">Add Manager</button>
			    </div>
			  </div>
		    """)))}),format.raw/*42.8*/("""
		"""),format.raw/*43.3*/("""</div>
    </fieldset>
""")))}))}
  }

  def render(page:String,isLoggedIn:Boolean,userInfo:User,managerForm:Form[views.formdata.ManagerForm]): play.twirl.api.HtmlFormat.Appendable = apply(page,isLoggedIn,userInfo,managerForm)

  def f:((String,Boolean,User,Form[views.formdata.ManagerForm]) => play.twirl.api.HtmlFormat.Appendable) = (page,isLoggedIn,userInfo,managerForm) => apply(page,isLoggedIn,userInfo,managerForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Tue Mar 24 18:17:11 PDT 2015
                  SOURCE: /Users/sagarbendale/Documents/workspace/pms/app/views/addmanager.scala.html
                  HASH: fcad5e83db173f08e16ad9a806ba9960536cd8e5
                  MATRIX: 774->1|1007->99|1034->149|1061->151|1091->173|1130->175|1158->177|1188->182|1212->186|1282->231|1360->301|1398->302|1434->311|1481->332|1615->445|1654->457|1785->567|1824->579|1957->691|2009->715|2505->1184|2639->1297|2678->1308|2916->1516|2946->1519
                  LINES: 26->1|30->1|31->4|32->5|32->5|32->5|33->6|33->6|33->6|36->9|36->9|36->9|37->10|38->11|40->13|41->14|43->16|44->17|46->19|48->21|61->34|63->36|64->37|69->42|70->43
                  -- GENERATED --
              */
          