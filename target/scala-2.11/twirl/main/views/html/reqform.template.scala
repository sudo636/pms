
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object reqform extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,Boolean,User,Form[views.formdata.RequestForm],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(page: String, isLoggedIn: Boolean, userInfo: User,requestForm: Form[views.formdata.RequestForm]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._
import views.html.bootstrap3._

Seq[Any](format.raw/*1.99*/("""

"""),format.raw/*5.1*/("""	 
"""),_display_(/*6.2*/main(page, isLoggedIn)/*6.24*/ {_display_(Seq[Any](format.raw/*6.26*/("""

    """),format.raw/*8.5*/("""<h3>Computer Service Department Account Request Form</h3>
    <fieldset>
    
	    <div class="">
		    """),_display_(/*12.8*/form(routes.Application.postForm(), 'class -> "form-horizontal")/*12.72*/{_display_(Seq[Any](format.raw/*12.73*/("""
		    	 
		  		"""),_display_(/*14.8*/text(requestForm("firstName"),
		        	label = "First Name",
		        	placeholder = "first name",
		        	help = "Please enter your first name. (required)")),format.raw/*17.62*/("""
		        	
	       		"""),_display_(/*19.12*/text(requestForm("lastName"),
		        	label = "Last Name",
		        	placeholder = "last name",
		        	help = "Please enter your last name. (required)")),format.raw/*22.61*/("""
		        	
	        	"""),_display_(/*24.12*/text(requestForm("empId"),
		        	label = "Employee/Student ID",
		        	placeholder = "employee or student ID",
		        	help = "Please enter your Student ID or Emplpyee ID whichever applicable. (required)")),format.raw/*27.98*/("""
		        	
	        	"""),_display_(/*29.12*/text(requestForm("contact"),
		        	label = "Your Contact #",
		        	placeholder = "contact number")),format.raw/*31.43*/("""
		        	
	        	"""),format.raw/*33.11*/("""<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button id="submit" type="submit" value="Submit" class="btn btn-primary">Request Access</button>
			    </div>
			  </div>
		    """)))}),format.raw/*38.8*/("""
		"""),format.raw/*39.3*/("""</div>
    </fieldset>
""")))}),format.raw/*41.2*/("""
"""))}
  }

  def render(page:String,isLoggedIn:Boolean,userInfo:User,requestForm:Form[views.formdata.RequestForm]): play.twirl.api.HtmlFormat.Appendable = apply(page,isLoggedIn,userInfo,requestForm)

  def f:((String,Boolean,User,Form[views.formdata.RequestForm]) => play.twirl.api.HtmlFormat.Appendable) = (page,isLoggedIn,userInfo,requestForm) => apply(page,isLoggedIn,userInfo,requestForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Tue Mar 24 18:17:11 PDT 2015
                  SOURCE: /Users/sagarbendale/Documents/workspace/pms/app/views/reqform.scala.html
                  HASH: d9c6a05fbbadef529eb3dc02ea685cc55350f38f
                  MATRIX: 771->1|1002->98|1030->149|1059->153|1089->175|1128->177|1160->183|1291->288|1364->352|1403->353|1446->370|1631->534|1682->558|1863->718|1914->742|2152->959|2203->983|2332->1091|2383->1114|2624->1325|2654->1328|2708->1352
                  LINES: 26->1|30->1|32->5|33->6|33->6|33->6|35->8|39->12|39->12|39->12|41->14|44->17|46->19|49->22|51->24|54->27|56->29|58->31|60->33|65->38|66->39|68->41
                  -- GENERATED --
              */
          