
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,Boolean,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String, isLoggedIn: Boolean)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.53*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>

<html>
    <head>
        <title>"""),_display_(/*7.17*/title),format.raw/*7.22*/(""" """),format.raw/*7.23*/("""hello</title>
         <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <link rel="stylesheet" media="screen" href=""""),_display_(/*9.54*/routes/*9.60*/.Assets.at("stylesheets/main.css")),format.raw/*9.94*/("""">
        <link rel="shortcut icon" type="image/png" href=""""),_display_(/*10.59*/routes/*10.65*/.Assets.at("images/favicon.png")),format.raw/*10.97*/("""">
        <script src=""""),_display_(/*11.23*/routes/*11.29*/.Assets.at("javascripts/hello.js")),format.raw/*11.63*/("""" type="text/javascript"></script>
        
        
       
        <!-- Bootstrap imports -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<!-- End Bootstrap imports -->
    </head>
    <body>
    
    <nav class="navbar navbar-default navbar-static-top">
	  <div class="container">
	     <div class="container-fluid">
		    <div class="navbar-header">
		      
		        <img alt="Brand" src=""""),_display_(/*28.34*/routes/*28.40*/.Assets.at("images/sjsu_logo.png")),format.raw/*28.74*/("""" class="navbar-brand">
		      	<ul class="nav navbar-nav">
			      	<li><a href=""""),_display_(/*30.25*/if(isLoggedIn)/*30.39*/{_display_(Seq[Any](format.raw/*30.40*/("""
			      		"""),format.raw/*31.12*/("""/dashboard
			      	""")))}),format.raw/*32.12*/("""
			      	"""),_display_(/*33.12*/if(!isLoggedIn)/*33.27*/{_display_(Seq[Any](format.raw/*33.28*/("""
			      		"""),format.raw/*34.12*/("""/
			      	""")))}),format.raw/*35.12*/(""""><strong>Account Request System</strong></a></li>
			        
			        """),_display_(/*37.13*/if(!isLoggedIn)/*37.28*/{_display_(Seq[Any](format.raw/*37.29*/("""
			        	"""),format.raw/*38.13*/("""<li class="active"><a href="/form">Form</a></li>
				        <li><a href="#">About</a></li>
				        <li><a href="#">Help</a></li>
				        <li><a href="#">Track Request</a></li>
			        """)))}),format.raw/*42.13*/("""
			         """),_display_(/*43.14*/if(isLoggedIn)/*43.28*/ {_display_(Seq[Any](format.raw/*43.30*/("""
			         	  """),format.raw/*44.16*/("""<li><a href="/data/add">Add Data Roles</a></li>
						  <li><a href="/data">View Data Roles</a></li>
						  <li><a href="/manager">View Managers</a></li>
						  <li><a href="/manager/add">Add Managers</a></li>
						  <li class=""><a href=""""),_display_(/*48.32*/routes/*48.38*/.Application.logout()),format.raw/*48.59*/("""">Logout</a></li>
			          """)))}),format.raw/*49.15*/("""
		      """),format.raw/*50.9*/("""</ul>
		    </div>
		  </div>
    		    
    		    
    		  
    		    
	     
	  </div>
	</nav>
	
	<div class="container">
	    """),_display_(/*62.7*/content),format.raw/*62.14*/("""
	"""),format.raw/*63.2*/("""</div>
    
        
        
        
    </body>
</html>
"""))}
  }

  def render(title:String,isLoggedIn:Boolean,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,isLoggedIn)(content)

  def f:((String,Boolean) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,isLoggedIn) => (content) => apply(title,isLoggedIn)(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Tue Mar 24 18:17:11 PDT 2015
                  SOURCE: /Users/sagarbendale/Documents/workspace/pms/app/views/main.scala.html
                  HASH: 081bcb19b98375f3ed47091305a6d02a73e8b1a8
                  MATRIX: 735->1|874->52|902->54|979->105|1004->110|1032->111|1197->250|1211->256|1265->290|1353->351|1368->357|1421->389|1473->414|1488->420|1543->454|2230->1114|2245->1120|2300->1154|2412->1239|2435->1253|2474->1254|2514->1266|2567->1288|2606->1300|2630->1315|2669->1316|2709->1328|2753->1341|2855->1416|2879->1431|2918->1432|2959->1445|3187->1642|3228->1656|3251->1670|3291->1672|3335->1688|3605->1931|3620->1937|3662->1958|3725->1990|3761->1999|3917->2129|3945->2136|3974->2138
                  LINES: 26->1|29->1|31->3|35->7|35->7|35->7|37->9|37->9|37->9|38->10|38->10|38->10|39->11|39->11|39->11|56->28|56->28|56->28|58->30|58->30|58->30|59->31|60->32|61->33|61->33|61->33|62->34|63->35|65->37|65->37|65->37|66->38|70->42|71->43|71->43|71->43|72->44|76->48|76->48|76->48|77->49|78->50|90->62|90->62|91->63
                  -- GENERATED --
              */
          