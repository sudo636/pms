package controllers;

import models.User;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

public class Secured extends Security.Authenticator{

	@Override
	public Result onUnauthorized(Context context) {
	  return redirect(routes.Application.login()); 
	}
	
	public static String getUser(Context ctx) {
	    return ctx.session().get("username");
	}
	
	public static boolean isLoggedIn(Context ctx) {
	    return (getUser(ctx) != null);
	}
	
	public static User getUserInfo(Context ctx){
		return (isLoggedIn(ctx)? User.getUserInfo(getUser(ctx)): null);
	}
}
