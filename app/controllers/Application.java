package controllers;

import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.formdata.LoginForm;
import views.formdata.ManagerForm;
import views.formdata.RequestForm;
import views.html.dashboard;
import views.html.index;
import views.html.login;
import views.html.reqform;
import views.html.addmanager;
import views.html.manager;
import views.html.addData;

public class Application extends Controller {

    public static Result index() {
    	session().clear();
    	System.out.println(Secured.isLoggedIn(ctx()));
    	return ok( index.render( "Home", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()) ) );
    }
    
    public static Result form(){
    	Form<RequestForm> requestForm = Form.form(RequestForm.class);
    	return ok(reqform.render("Home", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()),requestForm));
    }
    
    public static Result postForm(){
    	Form<RequestForm> requestForm = Form.form(RequestForm.class);
    	return ok();
    }
    
    //admin login
    public static Result login(){
    	Form<LoginForm> loginForm = Form.form(LoginForm.class);
    	return ok(login.render("Home", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()) , loginForm));
    }
    
    //validate login here
    public static Result postLogin(){
    	Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();
    	String username = loginForm.get().username;
    	String password = loginForm.get().password;
    	if( User.authenticate(username, password) ){
    		session().clear();
    		session("username", username);
    		return redirect(routes.Application.dashboard());
    	}
    	return redirect(routes.Application.index());
    }
    
    @Security.Authenticated(Secured.class)
    public static Result logout() {
      session().clear();
      return redirect(routes.Application.index());
    }
    
    //dashboard
    @Security.Authenticated(Secured.class)
    public static Result dashboard(){
    	return ok(dashboard.render("Dashboard", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())) );
    }
    
    
    //Add all managers
    //@Security.Authenticated(Secured.class)
    public static Result addManager(){
    	Form<ManagerForm> managerForm = Form.form(ManagerForm.class);
    	return ok( addmanager.render( "Add Manager", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()), managerForm  ));
    }
    
    //form submission
    //@Security.Authenticated(Secured.class)
    public static Result postAddManager(){
    	return ok();
    }
    
    //@Security.Authenticated(Secured.class)
    public static Result allManagers(){
    	return ok(manager.render("All Managers", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())) );
    }

    public static Result addData(){
    	return ok(addData.render("Add Data", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())) );
    }
    
}
