package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model.Finder;

@Entity
@Table(name="manager")
public class Manager {

	@Id
	public int id;
	
	@Required
	public String firstName;
	
	@Required
	public String lastName;
	
	@Required
	public String email;
	
	public String contact;
	
	@ManyToMany(cascade = CascadeType.ALL)
	public List<Data> dataRoles = new ArrayList<Data>();
	
	public static Finder<Integer, Manager> find = new Finder<Integer, Manager>(Integer.class, Manager.class);
	
}
