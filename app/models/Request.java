package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.db.ebean.Model.Finder;

@Entity
public class Request {
	@Id
	public long id;
	public String firstName;
	public String lastName;
	public int empId;
	public String contact;
	
	public static Finder<Long,Request> find = new Finder<Long, Request>(Long.class, Request.class);
	
}
