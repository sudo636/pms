package models;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.ExpressionList;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model.Finder;

@Entity
@Table(name="user")
public class User {
	
	@Id
	public long id;
	
	@Required
	@Column(unique=true)
	public String username;
	
	@Required
	public String password;
	
	public String name;
	
	public static Finder<Long,User> find = new Finder<Long, User>(Long.class, User.class);
	
	private static String getMD5(String s){
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m.update(s.getBytes(),0,s.length());
		return new BigInteger(1,m.digest()).toString(16);
	}
	
	public static boolean authenticate(String username, String password){
		System.out.println(User.find.where().eq("username", username).eq("password",getMD5(password) ).findRowCount());
		if( User.find.where().eq("username", username).eq("password",getMD5(password) ).findRowCount() == 1){
			return true;
		}
		return false;
	}
	
	public static User getUserInfo(String username){
		return User.find.where().eq("username", username).findUnique();
	}
}
