package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model.Finder;

@Entity
public class Data {
	@Id
	public int id;
	
	@Required
	public String driveName;
	
	public String name;
	
	public String description;
	
	public static Finder<Integer, Data> find = new Finder<Integer, Data>(Integer.class, Data.class);
	
}
